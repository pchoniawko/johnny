// przypisujemy johnny-five do const five
const five = require("johnny-five");

// tworzymy instancje płytki arduino
const board = new five.Board();


// za raz po uruchomieniu serwera na domyslnym porcie usb zostaje wykonana funkcja 
// callback w której wykonujemy zapalanie i gasniecie diody co 2000ms
board.on("ready", function () {
    new five.Led(13).strobe(2000);
});


/* 
############
W przypadku zmiany domyslnego portu podajemy go w konstruktorze:
const board = new five.Board({
    port: "nazwa/portu"
});
############

############
instrukcje przed uruchomieniem: https://github.com/rwaldron/johnny-five/wiki/Getting-Started
############

*/

